package com.firstproject.vinhos.model;

public enum TipoVinho {
    TINTO, BRANCO, ROSE
}
