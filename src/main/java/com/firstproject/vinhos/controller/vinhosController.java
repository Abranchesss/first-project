package com.firstproject.vinhos.controller;

import com.firstproject.vinhos.model.TipoVinho;
import com.firstproject.vinhos.model.Vinho;
import com.firstproject.vinhos.repository.Vinhos;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/vinhos")
public class vinhosController {

    @Autowired
    private Vinhos vinhos;

    @DeleteMapping("/{id}")
    public String remover(@PathVariable Long id, RedirectAttributes attributes){
        vinhos.deleteById(id);
        attributes.addFlashAttribute("message", "Vinho deletado com sucesso!");

        return "redirect:/vinhos";
    }

    @GetMapping()
    public ModelAndView listar(){
        ModelAndView modelAndView = new ModelAndView("vinhos/lista-vinhos");
        modelAndView.addObject("vinhos", vinhos.findAll());
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView editar(@PathVariable Long id) {
        return novo(vinhos.getById(id));
    }

    @GetMapping("/novo")
    public ModelAndView novo(Vinho vinho){
        ModelAndView modelAndView = new ModelAndView("vinhos/cadastro-vinho");

        modelAndView.addObject(vinho);
        modelAndView.addObject("tipos", TipoVinho.values());

        return modelAndView;
    }

    @PostMapping("/novo")
    public ModelAndView salvar(@Valid Vinho vinho, BindingResult result, RedirectAttributes attributes){
        System.out.print(vinho.getId());
        if(result.hasErrors()){
            return novo(vinho);
        }
        vinhos.save(vinho);
        System.out.print(vinho.getId());
        attributes.addFlashAttribute("message", "Vinho salvo com sucesso!");
        return new ModelAndView("redirect:/vinhos/novo");
    }
}
