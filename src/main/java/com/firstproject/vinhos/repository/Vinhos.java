package com.firstproject.vinhos.repository;

import com.firstproject.vinhos.model.Vinho;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Vinhos extends JpaRepository<Vinho, Long> {

}
